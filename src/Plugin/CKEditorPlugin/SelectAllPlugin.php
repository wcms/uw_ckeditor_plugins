<?php

namespace Drupal\uw_ckeditor_plugins\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Select All" plugin.
 *
 * @CKEditorPlugin(
 *   id = "selectall",
 *   label = @Translation("Select All"),
 *   module = "uw_ckeditor_plugins"
 * )
 */
class SelectAllPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getModulePath('uw_ckeditor_plugins') . '/libraries/selectall/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'SelectAll' => [
        'label' => $this->t('Select All'),
        'image' => $this->getModulePath('uw_ckeditor_plugins') . '/libraries/selectall/icons/selectall.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
