<?php

namespace Drupal\uw_ckeditor_plugins\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "find" plugin.
 *
 * @CKEditorPlugin(
 *   id = "find",
 *   label = @Translation("Find/Replace Plugin"),
 * )
 */
class FindPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Find' => [
        'label' => $this->t('Find'),
        'image' => $this->getModulePath('uw_ckeditor_plugins') . '/libraries/find/icons/find.png',
      ],
      'Find RTL' => [
        'label' => $this->t('Find RTL'),
        'image' => $this->getModulePath('uw_ckeditor_plugins') . '/libraries/find/icons/find-rtl.png',
      ],
      'Replace' => [
        'label' => $this->t('Replace'),
        'image' => $this->getModulePath('uw_ckeditor_plugins') . '/libraries/find/icons/replace.png',
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getModulePath('uw_ckeditor_plugins') . '/libraries/find/plugin.js';
  }

}
