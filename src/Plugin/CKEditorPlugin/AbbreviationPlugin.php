<?php

namespace Drupal\uw_ckeditor_plugins\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "abbr" plugin.
 *
 * @CKEditorPlugin(
 *   id = "abbr",
 *   label = @Translation("Abbreviation ckeditor button")
 * )
 */
class AbbreviationPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getModulePath('uw_ckeditor_plugins') . '/libraries/abbr/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'abbr' => [
        'label' => $this->t('Abbreviation'),
        'image' => $this->getModulePath('uw_ckeditor_plugins') . '/libraries/abbr/icons/abbr.png',
      ],
    ];
  }

}
