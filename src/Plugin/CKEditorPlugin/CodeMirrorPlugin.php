<?php

namespace Drupal\uw_ckeditor_plugins\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "codemirror" plugin.
 *
 * @CKEditorPlugin(
 *   id = "codemirror",
 *   label = @Translation("CodeMirror Plugin"),
 * )
 */
class CodeMirrorPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getModulePath('uw_ckeditor_plugins') . '/libraries/codemirror/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'CodeMirror' => [
        'label' => $this->t('CodeMirror'),
        'image' => $this->getModulePath('uw_ckeditor_plugins') . '/libraries/codemirror/icons/autoformat.png',
      ],
    ];
  }

}
