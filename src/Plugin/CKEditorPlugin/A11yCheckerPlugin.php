<?php

namespace Drupal\uw_ckeditor_plugins\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "a11ychecker" plugin.
 *
 * @CKEditorPlugin(
 *   id = "a11ychecker",
 *   label = @Translation("Accessibility Checker")
 * )
 */
class A11yCheckerPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return ['balloonpanel'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return ['core/drupal.jquery'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'A11ychecker' => [
        'label' => $this->t('Accessibility Checker'),
        'image' => $this->getModulePath('uw_ckeditor_plugins') . '/libraries/a11ychecker/icons/a11ychecker.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getModulePath('uw_ckeditor_plugins') . '/libraries/a11ychecker/plugin.js';
  }

}
