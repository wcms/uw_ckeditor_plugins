<?php

namespace Drupal\uw_ckeditor_plugins\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "textselection" plugin.
 *
 * @CKEditorPlugin(
 *   id = "textselection",
 *   label = @Translation("TextSelection Plugin"),
 * )
 */
class TextSelectionPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getModulePath('uw_ckeditor_plugins') . '/libraries/textselection/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'TextSelection' => [
        'label' => $this->t('TextSelection'),
        'image' => $this->getModulePath('uw_ckeditor_plugins') . '/libraries/textselection/icons/textselection.png',
      ],
    ];
  }

}
