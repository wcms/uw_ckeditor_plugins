/**
 * @file
 * The abbr language string definitions.
 */

"use strict"

CKEDITOR.plugins.setLang('abbr', 'en', {
  buttonTitle: 'Insert abbreviation',
  menuItemTitle: 'Edit abbreviation',
  dialogTitle: 'Abbreviation properties',
  dialogAbbreviationTitle: 'Abbreviation',
  dialogExplanationTitle: 'Explanation'
});
